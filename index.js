// console.log("Hello World!");

// fetch()
// this is a method in JavaScript that is used to send request in the server and load the information (response from the server) in the web pages. (JSON format)

// Syntax:
// fetch("urlAPI", {options})
// url - the url which the request is to be made.
// options - an array of properties, optional parameter.

/* 
   Endpoint of the API:
   GET and POST
   /movies

   UPDATE and DELETE:
   /movies/:id
*/

let movies;
let fetchMovies = () => {
  movies = fetch("https://sheltered-hamlet-48007.herokuapp.com/movies")
    .then((response) => response.json())
    .then((data) => showPost(data));
};
fetchMovies();
// Show fetch data in the doucment (HTML web page)

const showPost = (movies) => {
  // Create a variable that will contain all the movies.
  let movieEntries = "";
  // forEach() to loop through each movies object.
  movies.map((movie) => {
    //   onclick() is an event occurs when the user click on an element.
    // This allows us to execute a JavaScript's function when an element gets click.

    //   We can assign HTML elements in JS Variables.
    movieEntries += `
      <div id="movie-${movie._id}">
         <h3 id="movie-title-${movie._id}">${movie.title}</h3>
         <p id="movie-desc-${movie._id}">${movie.description}</p>
         <button onclick="editMovie('${movie._id}')">Edit</button>
         <button onclick="deleteMovie('${movie._id}')">Delete</button>
      </div>
     `;
  });
  console.log(movies);
  //   console.log(movieEntries);
  document.querySelector("#div-movie-entries").innerHTML = movieEntries;
  //
};
let title = document.querySelector("#txt-title");
let description = document.querySelector("#txt-desc");
// Add movie data
document.querySelector("#form-add-movie").addEventListener("submit", (e) => {
  // Prevents the page from loading
  e.preventDefault();

  let title = document.querySelector("#txt-title").value;
  let description = document.querySelector("#txt-desc").value;

  fetch("https://sheltered-hamlet-48007.herokuapp.com/movies", {
    method: "POST",
    body: JSON.stringify({
      title: title,
      description: description,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      // console.log(data);
      alert("Succesfully added.");
      //  query selector = null resets the state of our input into blanks after submitting a new post.
      document.querySelector("#txt-title").value = null;
      document.querySelector("#txt-desc").value = null;
      //  To show new movies
      fetchMovies();
      // lastname movie; sample description.
    });
});

// Edit Movie Button
// Create the JS Function called in the onclick event.
const editMovie = (id) => {
  console.log(id);
  let title = document.querySelector(`#movie-title-${id}`).innerHTML;
  let description = document.querySelector(`#movie-desc-${id}`).innerHTML;

  // pass the id, title, and description in the Edit form and enable the update button.
  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-desc").value = description;
  document.querySelector("#txt-edit-title").removeAttribute("disabled");
  document.querySelector("#txt-edit-desc").removeAttribute("disabled");
  document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

// Update a movie
// This will trigger an event that will update a certain movie upon clicking the update button.

document.querySelector("#form-edit-movie").addEventListener("submit", (e) => {
  e.preventDefault();
  let id = document.querySelector("#txt-edit-id").value;
  fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
    method: "PUT",
    body: JSON.stringify({
      title: document.querySelector("#txt-edit-title").value,
      description: document.querySelector("#txt-edit-desc").value,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Succesfully updated.");
      document.querySelector("#txt-edit-id").value = null;
      document.querySelector("#txt-edit-title").value = null;
      document.querySelector("#txt-edit-desc").value = null;

      document.querySelector("#txt-edit-title").setAttribute("disabled", true);
      document.querySelector("#txt-edit-desc").setAttribute("disabled", true);
      document
        .querySelector("#btn-submit-update")
        .setAttribute("disabled", true);
      fetchMovies();
    });
});

// Delete Movie Button
/* 
  .remove() method it removes an element (or node) from the HTML document.
*/

const deleteMovie = (id) => {
  console.log(id);

  fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully Deleted");
    });
  document.querySelector(`#movie-${id}`).remove();
};
